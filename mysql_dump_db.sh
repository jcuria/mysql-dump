#!/bin/bash

_backup_path=""
_backup_file="database.$(date +"%Y%m%d").sql"
_database=""
_user=""
_pass=""

#Dump database
mysqldump --user=$_user --password=$_pass --single-transaction $_database > $_backup_path/$_backup_file

#Delete backup files older than 7 days
find $_backup_path/* -type f -name '*.sql' -mtime +7 -exec rm {} \;
